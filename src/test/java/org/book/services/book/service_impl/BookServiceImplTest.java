package org.book.services.book.service_impl;

import org.book.exceptions.RestExceptions;
import org.book.exceptions.ServiceExceptions;
import org.book.model.book.BookEntity;
import org.book.model.book.BookRepository;
import org.book.services.book.service_api.BookService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = BookServiceImpl.class)
@RunWith(SpringRunner.class)
public class BookServiceImplTest {
    @MockBean
    BookRepository bookRepository;

    @Autowired
    BookService bookService;

    @Test
    public void getBook_andExpectReturnBook() {
        BookEntity bookEntity = new BookEntity();
        bookEntity.setName("java");

        when(bookRepository.findByName(bookEntity.getName())).thenReturn(Optional.of(bookEntity));

        BookEntity response = this.bookService.getBook("java");

        assertEquals(response.getName(), bookEntity.getName());
    }

    @Test
    public void saveBook() {
        BookEntity bookEntity = new BookEntity();
        bookEntity.setName("java");

        when(bookRepository.findByName(anyString())).thenReturn(Optional.empty());
        when(bookRepository.save(Mockito.any())).then(r -> (r.getArgument(0)));

        BookEntity response = bookService.saveBook(bookEntity);

        assertNotNull(response);
        assertEquals(response.getName(), bookEntity.getName());
    }

    @Test(expected = ServiceExceptions.InstanceFoundException.class)
    public void saveBook_andInstanceFoundException() {

        BookEntity bookEntity = new BookEntity();
        bookEntity.setName("java");

        when(bookRepository.findByNameIgnoreCase(anyString())).thenReturn(Optional.of(bookEntity));

        bookService.saveBook(bookEntity);

    }

    @Test
    public void deleteBook() {
        BookEntity bookEntity = new BookEntity();
        bookEntity.setName("java");

        when(bookRepository.findByName(anyString())).thenReturn(Optional.of(bookEntity));
        when(bookRepository.deleteByName(anyString())).thenReturn(bookEntity);

        BookEntity response = bookService.deleteBook("bookName");

        assertNotNull(response);
        assertEquals(response.getName(), bookEntity.getName());


    }

    @Test(expected = ServiceExceptions.InstanceNotFoundException.class)
    public void deleteBook_andInstanceNotFoundException() {

        BookEntity bookEntity = new BookEntity();
        bookEntity.setName("java");

        when(bookRepository.findByName(anyString())).thenReturn(Optional.empty());
        when(bookRepository.deleteByName(anyString())).thenReturn(bookEntity);

        BookEntity response = bookService.deleteBook("bookName");

    }

    @Test
    public void updateBook() {
        BookEntity bookEntity1 = new BookEntity();
        bookEntity1.setName("java");

        BookEntity bookEntity2 = new BookEntity();
        bookEntity1.setName("java1");


        when(bookRepository.findByName(anyString())).thenReturn(Optional.of(bookEntity1));
        when(bookRepository.save(Mockito.any())).then(r -> (r.getArgument(0)));

        BookEntity response = bookService.updateBook(bookEntity2, "name");

        assertNotNull(response);
        assertEquals(response.getName(), bookEntity2.getName());
    }


    @Test(expected = RestExceptions.EntityNotFoundException.class)
    public void updateBook_andExpectEntityNotFound() {

        BookEntity bookEntity = new BookEntity();
        bookEntity.setName("java");

        when(bookRepository.findByName(anyString())).thenReturn(Optional.empty());

        BookEntity response = bookService.updateBook(bookEntity, "name");

    }
}