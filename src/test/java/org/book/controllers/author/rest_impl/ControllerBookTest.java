package org.book.controllers.author.rest_impl;

import org.book.controllers.author.rest_api.BookDto;
import org.book.model.book.BookEntity;
import org.book.services.book.service_api.BookService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest
@ContextConfiguration(classes = {ControllerBook.class, BookMapperImpl.class})
@AutoConfigureMockMvc()
public class ControllerBookTest {
    @MockBean
    BookService bookService;
    @Autowired
    private MockMvc mvc;

    @Test
    public void getBook_andExpectStatusOk() throws Exception {
        BookDto bookDto = new BookDto();
        bookDto.setName("java");

        BookEntity bookEntity = new BookEntity();
        bookEntity.setName("java");

        when(bookService.getBook(anyString())).thenReturn(bookEntity);

        mvc.perform(get("/book/java"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(bookDto.getName()));
    }
}