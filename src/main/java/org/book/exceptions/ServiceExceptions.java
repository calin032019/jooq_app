package org.book.exceptions;

public final class ServiceExceptions extends Throwable {
    public static final class InsertFailedException extends RuntimeException {
        public InsertFailedException() {}

        public InsertFailedException(String message) { super(message); }
    }

    public static final class InstanceNotFoundException extends RuntimeException {
        public InstanceNotFoundException() {}

        public InstanceNotFoundException(String message) { super(message); }
    }

    public static final class InstanceFoundException extends RuntimeException {
        public InstanceFoundException() {}

        public InstanceFoundException(String message) { super(message); }
    }

    public static final class MaxFailAttempt extends RuntimeException {
        public MaxFailAttempt() {}

        public MaxFailAttempt(String message) { super(message); }
    }

    public static final class DeleteFailedException extends RuntimeException {
        public DeleteFailedException() {}

        public DeleteFailedException(String message) { super(message); }
    }

    public static final class InconsistentDataException extends RuntimeException {
        public InconsistentDataException() {}

        public InconsistentDataException(String message) { super(message); }
    }
}
