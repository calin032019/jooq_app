package org.book.controllers.author.rest_api;

import org.book.model.book.BookEntity;
import org.springframework.stereotype.Component;

@Component
public interface BookMapper {
    BookDto asBookDto(BookEntity in);

    BookEntity asBookEntity(BookDto in);
}
