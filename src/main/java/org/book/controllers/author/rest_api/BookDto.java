package org.book.controllers.author.rest_api;

import lombok.Getter;
import lombok.Setter;
import java.util.List;

@Setter
@Getter
public class BookDto {

    private String name;

    private List<String> authors;
}
