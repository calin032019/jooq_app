package org.book.controllers.author.rest_api;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("book")
public interface BookEndpoint {

    @GetMapping(path = "/{name}")
    BookDto getBook(@PathVariable("name") String name);

    @GetMapping
    List<BookDto> getAllBooks();

    @PostMapping
    BookDto saveBook(@RequestBody BookDto dto);

    @PutMapping("/{name}")
    BookDto updateBook(@RequestBody BookDto dto, @PathVariable(name = "name") String name);

    @DeleteMapping("/{name}")
    BookDto deleteBook(@PathVariable("name") String email);
}
