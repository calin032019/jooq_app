package org.book.controllers.author.rest_impl;

import org.book.controllers.author.rest_api.BookDto;
import org.book.controllers.author.rest_api.BookEndpoint;
import org.book.controllers.author.rest_api.BookMapper;
import org.book.exceptions.RestExceptions;
import org.book.exceptions.ServiceExceptions;
import org.book.model.book.BookEntity;
import org.book.services.book.service_api.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ControllerBook implements BookEndpoint {
    @Autowired
    private BookService bookService;

    @Autowired
    private BookMapper bookMapper;

    @Override
    public BookDto getBook(String name) {
        try {
            return bookMapper.asBookDto(bookService.getBook(name));
        } catch (ServiceExceptions.InstanceNotFoundException e) {
            throw new RestExceptions.EntityNotFoundException(e.getMessage());
        }
    }

    @Override
    public List<BookDto> getAllBooks() {
        return this.bookService.getAllBooks().stream().map(bookEntity -> bookMapper.asBookDto(bookEntity)).collect(Collectors.toList());
    }

    @Override
    public BookDto saveBook(BookDto dto) {
        BookEntity bookEntity = bookMapper.asBookEntity(dto);
        try {
            BookEntity response = this.bookService.saveBook(bookEntity);
            return bookMapper.asBookDto(response);
        } catch (ServiceExceptions.InsertFailedException e) {
            throw new RestExceptions.BadRequest(e.getMessage());
        } catch (ServiceExceptions.InstanceFoundException e) {
            throw new RestExceptions.EntityExistsException(e.getMessage());
        }
    }

    @Override
    public BookDto updateBook(BookDto dto, String name) {
        BookEntity bookEntity = bookMapper.asBookEntity(dto);

        try {
            BookEntity response = bookService.updateBook(bookEntity, name);
            return bookMapper.asBookDto(response);
        } catch (ServiceExceptions.InsertFailedException e) {
            throw new RestExceptions.BadRequest(e.getMessage());
        } catch (ServiceExceptions.InstanceNotFoundException e) {
            throw new RestExceptions.EntityNotFoundException(e.getMessage());
        } catch (ServiceExceptions.InstanceFoundException e) {
            throw new RestExceptions.EntityExistsException(e.getMessage());
        }
    }

    @Override
    public BookDto deleteBook(String name) {
        try {
            BookEntity response = this.bookService.deleteBook(name);
            return bookMapper.asBookDto(response);
        } catch (ServiceExceptions.InstanceNotFoundException e) {
            throw new RestExceptions.EntityNotFoundException(e.getMessage());
        } catch (ServiceExceptions.DeleteFailedException e) {
            throw new RestExceptions.BadRequest(e.getMessage());
        }
    }

}
