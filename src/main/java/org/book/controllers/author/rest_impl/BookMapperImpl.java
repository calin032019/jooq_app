package org.book.controllers.author.rest_impl;

import org.book.controllers.author.rest_api.BookDto;
import org.book.controllers.author.rest_api.BookMapper;
import org.book.model.book.BookEntity;
import org.springframework.stereotype.Component;

@Component
public class BookMapperImpl implements BookMapper {

    @Override
    public BookDto asBookDto(BookEntity in) {
        BookDto bookDto = new BookDto();
        bookDto.setName(in.getName());

        return bookDto;
    }

    @Override
    public BookEntity asBookEntity(BookDto in) {
        BookEntity bookEntity = new BookEntity();
        bookEntity.setName(in.getName());
        return bookEntity;
    }


}
