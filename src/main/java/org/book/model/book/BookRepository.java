package org.book.model.book;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

public interface BookRepository extends JpaRepository<BookEntity, Long> {
    Optional<BookEntity> findByName(String name);

    boolean existsByName(String name);

    @Query("SELECT p from BookEntity p where LOWER(p.name) = LOWER(?1)")
    Optional<BookEntity> findByNameIgnoreCase(String name);

    BookEntity deleteByName(String name);
}
