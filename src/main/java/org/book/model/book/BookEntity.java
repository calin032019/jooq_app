package org.book.model.book;

import org.book.model.book_author.BookAuthorEntity;
import org.book.model.publisher.PublisherEntity;

import javax.persistence.*;

@Entity
public class BookEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    @ManyToOne
    private BookAuthorEntity bookAuthorEntity;

    @ManyToOne
    @JoinColumn(name = "PUBLISHER_ID")
    private PublisherEntity publisherEntity;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PublisherEntity getPublisherEntity() {
        return publisherEntity;
    }

    public void setPublisherEntity(PublisherEntity publisherEntity) {
        this.publisherEntity = publisherEntity;
    }
}
