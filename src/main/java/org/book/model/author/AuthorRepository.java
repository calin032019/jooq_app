package org.book.model.author;

import org.book.model.book_author.BookAuthorEntity;
import org.springframework.data.jpa.repository.JpaRepository;


public interface AuthorRepository extends JpaRepository<AuthorEntity, Long> {
    BookAuthorEntity findByName(String name);

    boolean existsByName(String name);
}
