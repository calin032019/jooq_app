package org.book.model.author;

import org.book.model.book_author.BookAuthorEntity;

import javax.persistence.*;
import java.util.List;

@Entity
public class AuthorEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private Integer version;

    @OneToMany
    private List<BookAuthorEntity> bookAuthorEntities;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public List<BookAuthorEntity> getBookAuthorEntities() {
        return bookAuthorEntities;
    }

    public void setBookAuthorEntities(List<BookAuthorEntity> bookAuthorEntities) {
        this.bookAuthorEntities = bookAuthorEntities;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
