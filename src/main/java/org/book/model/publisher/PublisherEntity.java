package org.book.model.publisher;

import org.book.model.book.BookEntity;

import javax.persistence.*;
import java.util.List;

@Entity
public class PublisherEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    @OneToMany(mappedBy = "publisherEntity")
    private List<BookEntity> bookEntities;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
