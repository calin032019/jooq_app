package org.book.model.publisher;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PublisherRepository extends JpaRepository<PublisherEntity, Long> {
    PublisherEntity findByName(String name);

    boolean existsByName(String name);
}
