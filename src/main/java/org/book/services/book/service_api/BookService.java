package org.book.services.book.service_api;

import org.book.model.book.BookEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface BookService {
    BookEntity getBook(String name);

    List<BookEntity> getAllBooks();

    public BookEntity saveBook(BookEntity book);

    public BookEntity updateBook(BookEntity book, String name);

    public BookEntity deleteBook(String name);
}
