package org.book.services.book.service_impl;

import org.book.exceptions.RestExceptions;
import org.book.exceptions.ServiceExceptions;
import org.book.model.book.BookEntity;
import org.book.model.book.BookRepository;
import org.book.services.book.service_api.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Component
public class BookServiceImpl implements BookService {
    @Autowired
    private BookRepository bookRepository;

    @Override
    public BookEntity getBook(String name) {
        Optional<BookEntity> book = this.bookRepository.findByName(name);
        if (!book.isPresent()) {
            throw new ServiceExceptions.InstanceNotFoundException("Book not found");
        }
        return book.get();
    }

    @Override
    public List<BookEntity> getAllBooks() {
        return bookRepository.findAll();
    }

    @Override
    public BookEntity saveBook(BookEntity book) {
        Optional<BookEntity> optional = bookRepository.findByNameIgnoreCase(book.getName().trim());

        if (optional.isPresent())
            throw new ServiceExceptions.InstanceFoundException("Book already exist");

        try {
            return this.bookRepository.save(book);
        } catch (Exception e) {
            throw new ServiceExceptions.InsertFailedException("Error inserting BookEntity");
        }
    }

    @Override
    public BookEntity updateBook(BookEntity book, String name) {
        Optional<BookEntity> entity = this.bookRepository.findByName(name);

        if (!entity.isPresent())
            throw new RestExceptions.EntityNotFoundException("Book does not exist");


        book.setId(entity.get().getId());

        try {
            return this.bookRepository.save(book);
        } catch (Exception e) {
            throw new ServiceExceptions.InsertFailedException("Error updating BookEntity");
        }
    }

    @Override
    public BookEntity deleteBook(String name) {
        Optional<BookEntity> entity = this.bookRepository.findByName(name);

        if (!entity.isPresent())
            throw new ServiceExceptions.InstanceNotFoundException("Book does not exist");

        try {
            this.bookRepository.delete(entity.get());
        } catch (Exception e) {
            throw new ServiceExceptions.DeleteFailedException("Error delete BookEntity");
        }

        return entity.get();
    }


}

